import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class DuplicateCharacters {
    
    static List<Character> findDuplicates(String str) {
        char[] charArray = str.toCharArray();
        Map<Integer, Character> map = new HashMap<>();
        List<Character> list = new ArrayList<>();
        
        for (int i = 0; i < charArray.length; i++) {
            if (!map.containsValue(charArray[i])) {
                map.put(i, charArray[i]);
            } else {
                list.add(charArray[i]);
            }
        }
        return list;
    }
    
    public static void main(String args[]) {
        List<Character> list = findDuplicates("FindDuplicateCharacters");
        System.out.print("Duplicate characters are - ");
        for(Character duplicates : list) {
            System.out.print(duplicates.charValue() + " ");
        }
    }
}