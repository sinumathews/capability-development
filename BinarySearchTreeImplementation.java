class BinarySearchTree {
    Node root;
    
    class Node {
        int value;
        Node left, right;
        
        Node(int value) {
            this.value = value;
            left = right = null;
        }
    }
    
    public void insert(int value) {
        Node newNode = new Node(value);
        if (root == null) {
            root = newNode;
        } else {
            Node currentNode = root;
            
            while(true) {
                if (value < currentNode.value ) {
                    if (null == currentNode.left) {
                        currentNode.left = newNode;
                        break;
                    }
                    currentNode = currentNode.left;
                } else {
                    if (null == currentNode.right) {
                        currentNode.right = newNode;
                        break;
                    }
                    currentNode = currentNode.right;
                }
            }
        }
    }
    
    public boolean lookup(int value) {
        if (root == null) {
            return false;
        } 
        Node currentNode = root;
        while(null != currentNode) {
            if (value < currentNode.value ) {
                currentNode = currentNode.left;
            } else if (value > currentNode.value ) { 
                currentNode = currentNode.right;
            } else if (value == currentNode.value ) {
                return true;
            }
        }
        return false;
    }
    
    void showItems() {  
        inorderRec(root);  
    } 
  
    void inorderRec(Node root) { 
        if (root != null) { 
            inorderRec(root.left); 
            System.out.println(root.value); 
            inorderRec(root.right); 
        } 
    } 
}

public class BinarySearchTreeImplementation {
    public static void main(String args[]) {
        BinarySearchTree bst = new BinarySearchTree();
        bst.insert(20);
        bst.insert(10);
        bst.insert(50);
        bst.insert(5);
        bst.insert(70);
        
        bst.showItems();
        
        System.out.println("Found item - " + bst.lookup(1000));
    }
}