
class ListNode {
  int val;
  ListNode next;
  ListNode() {}
  ListNode(int val) { this.val = val; }
  ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

public class AddTwoNumbersWithLinkedList {
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        String str1 = "";
        String str2 = "";
            
        while (l1.next != null) {
            ListNode node = l1;
            str1 = str1 + node.val;
            l1 = node.next;
        }
        str1 = str1 + l1.val;
            
        while (l2.next != null) {
            ListNode node = l2;
            str2 = str2 + node.val;
            l2 = node.next;
        }
        str2 = str2 + l2.val;
        
        int sum = Integer.parseInt(str1) + Integer.parseInt(str2);
        ListNode sumNode = new ListNode(sum);
        return sumNode;
    }
    
    public static void main(String[] args) {
        ListNode node1 = new ListNode(3);
        ListNode node2 = new ListNode(4, node1);
        ListNode node3 = new ListNode(2, node2);
        
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(6, node4);
        ListNode node6 = new ListNode(5, node5);  
        
        ListNode sumNode = addTwoNumbers(node3, node6);
        System.out.println("Sum - " + sumNode.val);
    }
}