import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

class Stack {
    Node top;
    Node bottom;
    int length;
    
    public int peek() {
        return this.top.value;
    }
    
    public void push(int value) {
        Node newNode = new Node(value);
        
        if (length == 0) {
            this.top = newNode;
            this.bottom = newNode;
        } else {
            Node node = this.top;
            newNode.next = node;
            this.top = newNode;
        }
        length++;
    }
    
    public void pop() {
        Node node = this.top.next;
        this.top.next = null;
        this.top = node;
        length--;
    }
    
    class Node {
        int value;
        Node next;
        
        Node (int value) {
            this.value = value;
        }
    }
}

public class StackImplementation {
    public static void main(String args[]) {
        Stack stack = new Stack();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        System.out.println("Top element - " + stack.peek());
        stack.pop();
        System.out.println("Top element after first pop operation - " + stack.peek());
        stack.pop();
        System.out.println("Top element after second pop operation - " + stack.peek());
    }
}