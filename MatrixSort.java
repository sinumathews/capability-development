import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class MatrixSort {
    
    public static int[][] sortDiagonally(int[][] array) {
      int row = array.length;
      int col = array[0].length;
      Map<Integer, List<Integer>> map = new HashMap<>();
      List<Integer> list = null;
      
      for (int i = 0; i < row; i++) {
          for (int j = 0; j < col; j++) {
              int diff = (i - j);
              if (!map.containsKey(diff)) {
                  list = new ArrayList<>();
                  map.put(diff, list);
              }
              list = map.get(diff);
              if (list != null) {
                  list.add(array[i][j]);
                  map.put(diff, list);
              }
          }
      }
      
      for(Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
        Integer key = entry.getKey();
        list = entry.getValue();
        Collections.sort(list);
        map.put(key, list);
      }
      
      for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
          int diff = (i - j);
          array[i][j]= map.get(diff).get(0);
          map.get(diff).remove(0);
        }
      }
      return array;
    }
    
    public static void main(String args[]) {
        int[][] array = { {4, 6, 2}, {5, 7, 1}, {9, 2, 8} };
        int[][] sortedArray = sortDiagonally(array);
        
        int row = sortedArray.length;
        int col = sortedArray[0].length;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
              System.out.println(sortedArray[i][j]);
            }
        }
    }
}