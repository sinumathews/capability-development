import java.util.Set;
import java.util.HashSet;

public class SetImplementation {
    public static void main(String args[]) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(4);
        set1.add(2);
        set1.add(9);
        set1.add(15);
        set1.add(12);
        System.out.println("Set1 - " + set1);
        
        // Convert Set to array
        Object[] arr = set1.toArray(); 
        System.out.println("Set1 to array - ");
        for (Object item : arr) {
            System.out.println(item);
        }
        
        Set<Integer> set2 = new HashSet<>();
        set2.add(5);
        set2.add(9);
        set2.add(8);
        set2.add(4);
        
        System.out.println("Set2 - " + set2);
        
        // Retain elements common to both Set1 and Set2
        set1.retainAll(set2);
        System.out.println("Retain Set1 & Set2 - " + set1);
        
        // Clear the elements of Set1
        set1.clear();
        System.out.println("Set1 - " + set1);
    }
}