import java.util.List;
import java.util.Arrays;

class LinkedList {
    ListNode head;
    
    public void initializeLinkedList(List<Integer> list) {
        if (head == null) {
            head = new ListNode();
        }
        
        if (null != list) {
            ListNode node = new ListNode(list.get(0));
            head.next = node;
            
            for (int i = 1; i < list.size(); i++) {
                ListNode newNode = new ListNode(list.get(i));
                node.next = newNode;
                
                if (i == (list.size() - 1)) {
                    newNode.next = null;
                }
                node = newNode;
            }
        }
    }
    
    public void deleteNode(int value) {
        ListNode node = this.head.next;
        ListNode previousNode = node;
        while(node.next != null) {
            if (node.val == value) {
                previousNode.next = node.next;
            } else {
                previousNode = node;
            }
            node = node.next;
        }
    }
    
    public void showItems() {
        ListNode node = this.head.next;
        System.out.print("Linked List items are - ");
        while(node.next != null) {
            System.out.print(" " + node.val);
            node = node.next;
        }
        System.out.print(" " + node.val + "\n");
    }
    
    class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int x) { val = x; }
    }
}
 
public class DeleteNodesInLinkedList {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        List<Integer> list = Arrays.asList(10, 20, 30, 40, 50, 60, 70, 80, 90, 100);
        
        linkedList.initializeLinkedList(list);
        linkedList.showItems();
        
        linkedList.deleteNode(40);
        linkedList.showItems();
        
        linkedList.deleteNode(80);
        linkedList.showItems();
    }
    
}